/*************************************************************************
 *  ./src/mcmc/GpuLikelihood.cpp
 *  Copyright Chris Jewell <chrism0dwk@gmail.com> 2012
 *
 *  This file is part of InFER.
 *
 *  InFER is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  InFER is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with InFER.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
/*
 * GpuLikelihood.cpp
 *
 *  Created on: Mar 19, 2012
 *      Author: stsiab
 */

#include <iostream>
#include <fstream>

#include <boost/numeric/ublas/matrix_sparse.hpp>
using namespace boost::numeric;

#include "Data.hpp"
#include "GpuLikelihood.hpp"

namespace EpiRisk
{

  size_t
  GpuLikelihood::GetNumInfecs() const
  {
    return hostInfecIdx_->size();
  }

  size_t
  GpuLikelihood::GetNumPossibleOccults() const
  {
    return hostSuscOccults_->size();
  }

  size_t
  GpuLikelihood::GetNumOccults() const
  {
    return hostInfecIdx_->size() - numKnownInfecs_;
  }

  
  

} // namespace EpiRisk

